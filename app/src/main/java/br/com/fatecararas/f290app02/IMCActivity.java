package br.com.fatecararas.f290app02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class IMCActivity extends AppCompatActivity {

    EditText peso, altura;
    TextView resultado;
    ImageView imagem;
    Button botao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);

        peso = findViewById(R.id.editTextPeso);
        altura = findViewById(R.id.editTextAltura);
        resultado = findViewById(R.id.textViewIMC);
        imagem = findViewById(R.id.imageViewIMC);
        botao = findViewById(R.id.buttonCalcularIMC);
    }

    public void calcularImc(View view){
        Float p = Float.parseFloat(peso.getText().toString());
        Float a = Float.parseFloat(altura.getText().toString());
        Double imc = p / Math.pow(a,2);
        resultado.setText(String.format("%.0f",imc));
    }
}
